using System;
using System.Threading.Tasks;
using TestWebApi.Database.Contexts.QueryContexts.Car;
using TestWebApi.Database.Interfaces;

namespace TestWebApi.Database.RepositoryQueries.Car
{
    public class GetByIdQuery : IRepositoryQuery<GetByIdQueryContext, Entities.Car>
    {
        public async Task<Entities.Car> QueryAsync(GetByIdQueryContext context)
        {
            return await context.CarRepository.GetByIdAsync(context.Id);;
        }
    }
}