using System.Collections.Generic;
using System.Threading.Tasks;
using TestWebApi.Database.Contexts.QueryContexts.Car;
using TestWebApi.Database.Interfaces;

namespace TestWebApi.Database.RepositoryQueries.Car
{
    public class GetCarsListQuery : IRepositoryQuery<GetCarsListQueryContext, List<Entities.Car>>
    {
        public async Task<List<Entities.Car>> QueryAsync(GetCarsListQueryContext context)
        {
            return await context.CarRepository.GetListAsync();
        }
    }
}