using System.Threading.Tasks;
using Autofac;
using TestWebApi.Database.Contexts.QueryContexts;
using TestWebApi.Database.Interfaces;

namespace TestWebApi.Database.RepositoryQueries
{
    public class QueryBuilder : IQueryBuilder
    {
        private static IComponentContext _componentContext;

        public QueryBuilder(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }

        public async Task<TReturnable> QueryAsync<TContext, TReturnable>(TContext context) where TContext : QueryContext
        {
            return await _componentContext.Resolve<IRepositoryQuery<TContext, TReturnable>>().QueryAsync(context);
        }
    }
}