namespace TestWebApi.Database.Constants
{
    public static class ResponseResultStatus
    {
        public static readonly string Ok = "Ok";

        public static readonly string Error = "Error";
    }
}