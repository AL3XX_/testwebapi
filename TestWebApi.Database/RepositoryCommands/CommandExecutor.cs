using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using TestWebApi.Database.Constants;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.RepositoryCommands
{
    public abstract class CommandExecutor
    {
        public static async Task<CommandResult> ExecuteAsync(Func<Task> action)
        {
            CommandResult result = new CommandResult();
            
            try
            {
                await action();

                result.Status = ResponseResultStatus.Ok;
                result.Message = "Command successfully executed!";
            }
            catch (Exception ex)
            {
                result.Status = ResponseResultStatus.Error;
                result.Message = $"Unable to execute command. {ex.Message}";
            }

            return result;
        }
    }
}