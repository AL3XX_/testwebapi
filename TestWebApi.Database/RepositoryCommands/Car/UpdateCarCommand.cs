using System;
using System.Threading.Tasks;
using TestWebApi.Database.Constants;
using TestWebApi.Database.Contexts.CommandContexts.Car;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.RepositoryCommands.Car
{
    public class UpdateCarCommand : IRepositoryCommand<UpdateCarCommandContext>
    {
        public async Task<CommandResult> ExecuteAsync(UpdateCarCommandContext commandContext)
        {        
            return await CommandExecutor.ExecuteAsync(async () =>
            {
                await commandContext.CarRepository.UpdateAsync(commandContext.Car);
            });
        }
    }
}