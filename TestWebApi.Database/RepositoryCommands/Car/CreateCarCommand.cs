using System;
using System.Threading.Tasks;
using TestWebApi.Database.Constants;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Contexts.CommandContexts.Car;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;
using TestWebApi.Database.Models.CustomCommandModels;

namespace TestWebApi.Database.RepositoryCommands.Car
{
    public class CreateCarCommand : IRepositoryCommand<CreateCarCommandContext>
    {   
        public async Task<CommandResult> ExecuteAsync(CreateCarCommandContext commandContext)
        {
            CreateCarCommandResult finalResult = new CreateCarCommandResult();
            
            CommandResult commandResult = await CommandExecutor.ExecuteAsync(async () =>
            {
                Entities.Car queryResult = await commandContext.CarRepository.CreateAsync(commandContext.Car);

                if (queryResult != null)
                {
                    finalResult.Id = queryResult.Id;
                }
            });

            finalResult.Status = commandResult.Status;
            finalResult.Message = commandResult.Message;

            return finalResult;
        }
    }
}