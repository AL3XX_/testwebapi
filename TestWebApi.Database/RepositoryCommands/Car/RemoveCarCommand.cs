using System.Threading.Tasks;
using TestWebApi.Database.Contexts.CommandContexts.Car;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.RepositoryCommands.Car
{
    public class RemoveCarCommand : IRepositoryCommand<RemoveCarCommandContext>
    {
        public async Task<CommandResult> ExecuteAsync(RemoveCarCommandContext commandContext)
        {
            return await CommandExecutor.ExecuteAsync(async () =>
            {
                await commandContext.CarRepository.RemoveAsync(commandContext.Id);
            });
        }
    }
}