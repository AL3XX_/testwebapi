using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Autofac;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Contexts.CommandContexts.Car;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.RepositoryCommands
{
    public class CommandBuilder : ICommandBuilder
    {
        private static IComponentContext _componentContext;

        public CommandBuilder(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }
        
        public async Task<CommandResult> ExecuteAsync<TContext>(TContext context) where TContext : CommandContext
        {
            return await _componentContext.Resolve<IRepositoryCommand<TContext>>().ExecuteAsync(context);
        }
    }
}