namespace TestWebApi.Database.Models
{
    public class CommandResult
    {
        public string Status { get; set; }
        
        public string Message { get; set; }
    }
}