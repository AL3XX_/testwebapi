namespace TestWebApi.Database.Models
{
    public class ResponseResult
    {   
        public string Status { get; set; }
        
        public string Message { get; set; }
    }
}