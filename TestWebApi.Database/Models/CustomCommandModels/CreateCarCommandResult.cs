using Newtonsoft.Json;

namespace TestWebApi.Database.Models.CustomCommandModels
{
    public class CreateCarCommandResult : CommandResult
    {
        [JsonProperty("car_id")]
        public string Id { get; set; }
    }
}