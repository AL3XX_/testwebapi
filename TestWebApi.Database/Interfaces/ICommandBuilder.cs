using System.Threading.Tasks;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.Interfaces
{
    public interface ICommandBuilder
    {
        Task<CommandResult> ExecuteAsync<TContext>(TContext context) where TContext : CommandContext;
    }
}