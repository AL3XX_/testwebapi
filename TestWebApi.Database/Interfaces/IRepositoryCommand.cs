using System.Threading.Tasks;
using MongoDB.Driver;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.Interfaces
{
    public interface IRepositoryCommand<in TContext> where TContext : CommandContext
    {
        Task<CommandResult> ExecuteAsync(TContext commandContext);
    }
}