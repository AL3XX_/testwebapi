using System.Threading.Tasks;

namespace TestWebApi.Database.Interfaces
{
    public interface IRepositoryQuery<in TContext, TReturnable>
    {
        Task<TReturnable> QueryAsync(TContext context);
    }
}