using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.IdGenerators;
using TestWebApi.Database.Contexts;
using TestWebApi.Database.Entities;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.Interfaces
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> CreateAsync(TEntity entity);

        Task<TEntity> GetByIdAsync(string id);
        
        Task<List<TEntity>> GetListAsync();

        Task<TEntity> UpdateAsync(TEntity entity);

        Task<Car> RemoveAsync(string entity);
    }
}