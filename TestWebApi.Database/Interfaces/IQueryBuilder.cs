using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TestWebApi.Database.Contexts.QueryContexts;
using TestWebApi.Database.Models;

namespace TestWebApi.Database.Interfaces
{
    public interface IQueryBuilder
    {
        Task<TReturnable> QueryAsync<TContext, TReturnable>(TContext context) where TContext : QueryContext;
    }
}