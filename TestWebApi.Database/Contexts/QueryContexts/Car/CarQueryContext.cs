using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Repositories;

namespace TestWebApi.Database.Contexts.QueryContexts.Car
{
    public class CarQueryContext : QueryContext
    {
        public IRepository<Entities.Car> CarRepository { get; set; }

        public CarQueryContext()
        {
            CarRepository = new CarRepository();
        }
    }
}