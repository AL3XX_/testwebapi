namespace TestWebApi.Database.Contexts.QueryContexts.Car
{
    public class GetByIdQueryContext : CarQueryContext
    {
        public string Id { get; set; }
    }
}