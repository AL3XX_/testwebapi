namespace TestWebApi.Database.Contexts.CommandContexts.Car
{
    public class CreateCarCommandContext : CarCommandContext
    {
        public Entities.Car Car { get; set; }
    }
}