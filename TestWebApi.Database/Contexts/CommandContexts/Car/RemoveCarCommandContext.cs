namespace TestWebApi.Database.Contexts.CommandContexts.Car
{
    public class RemoveCarCommandContext : CarCommandContext
    {
        public string Id { get; set; }
    }
}