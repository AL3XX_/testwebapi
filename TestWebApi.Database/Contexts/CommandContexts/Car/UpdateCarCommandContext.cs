namespace TestWebApi.Database.Contexts.CommandContexts.Car
{
    public class UpdateCarCommandContext : CarCommandContext
    {
        public Entities.Car Car { get; set; }
    }
}