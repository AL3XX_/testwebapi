using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Repositories;

namespace TestWebApi.Database.Contexts.CommandContexts.Car
{
    public class CarCommandContext : CommandContext
    {
        public IRepository<Entities.Car> CarRepository { get; set; }

        public CarCommandContext()
        {
            CarRepository = new CarRepository();
        }
    }
}