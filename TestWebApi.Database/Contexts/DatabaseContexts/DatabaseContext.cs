using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using TestWebApi.Database.Entities;

namespace TestWebApi.Database.Contexts.DatabaseContexts
{
    public class DatabaseContext
    {
        protected readonly IMongoDatabase Database;
        
        public IMongoCollection<Car> Cars => Database.GetCollection<Car>("Cars");

        public DatabaseContext()
        {            
            string connectionString = "mongodb://localhost:27017/WebApiDb";

            MongoUrlBuilder connection = new MongoUrlBuilder(connectionString);
            MongoClient client = new MongoClient(connectionString);
            
            Database = client.GetDatabase(connection.DatabaseName);
        }
    }
}