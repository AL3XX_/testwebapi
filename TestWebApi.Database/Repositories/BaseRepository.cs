using TestWebApi.Database.Contexts.DatabaseContexts;

namespace TestWebApi.Database.Repositories
{
    public abstract class BaseRepository
    {
        protected DatabaseContext DatabaseContext { get; set; }

        protected BaseRepository()
        {
            DatabaseContext = new DatabaseContext();
        }
    }
}