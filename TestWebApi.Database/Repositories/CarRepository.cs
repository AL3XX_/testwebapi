using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Serialization;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Entities;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;
using TestWebApi.Database.RepositoryCommands.Car;

namespace TestWebApi.Database.Repositories
{
    public class CarRepository : BaseRepository, IRepository<Car>
    {
        public async Task<Car> CreateAsync(Car entity)
        {
            if (entity != null)
            {
                entity.Id = ObjectId.GenerateNewId().ToString();

                if (!string.IsNullOrEmpty(entity.Name))
                {
                    await DatabaseContext.Cars.InsertOneAsync(entity);
                }
                else
                {
                    // Unable to create car without name.
                    throw new Exception("Unable to create car. Name is undefined.");
                }
            }

            return entity;
        }

        public async Task<Car> GetByIdAsync(string id)
        {
            Entities.Car car = null;

            try
            {
                IAsyncCursor<Car> result = await DatabaseContext.Cars.FindAsync(new BsonDocument("_id", new ObjectId(id)));
                car = await result.FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to find car with id {id}");
            }
            
            return car;
        }

        public async Task<List<Car>> GetListAsync()
        {
            return await DatabaseContext.Cars.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<Car> UpdateAsync(Car entity)
        {
            try
            {
                Car oldCar = await GetByIdAsync(entity.Id);
                
                // Getting old name if query doesn't update Name.
                if (string.IsNullOrEmpty(entity.Name))
                {
                    entity.Name = oldCar.Name;
                }

                // Forbid null description update
                if (entity.Description == null)
                {
                    entity.Description = oldCar.Description;
                }
                
                await DatabaseContext.Cars.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(entity.Id)), entity);
            }
            catch (Exception ex)
            {
                // Changing exception message to custom.
                throw new Exception("Unable to update car.");
            }

            return entity;
        }

        public async Task<Car> RemoveAsync(string id)
        {
            Car removedCar = await DatabaseContext.Cars.FindOneAndDeleteAsync(new BsonDocument("_id", new ObjectId(id)));

            if (removedCar == null)
            {
                throw new Exception($"Car {id} is not exists or was removed before.");
            }

            return removedCar;
        }
    }
}