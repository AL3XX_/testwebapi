using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TestWebApi.Database.Contexts.DatabaseContexts;
using TestWebApi.Database.Entities;
using Xunit;
using Xunit.Abstractions;

namespace TestWebApi.Tests
{
    public class CarControllerTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        
        private List<Car> CarsInDatabase { get; set; }

        public CarControllerTests(ITestOutputHelper testOutputHelper)
        {
            CreateTestDb();
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async Task CreateResultHasCorrectErrorResponse()
        {
            string requestData = JsonConvert.SerializeObject(GetRandomCarFromDb());
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Post, "create",
                requestData);

            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }

        [Fact]
        public async Task UpdateResultHasCorrectErrorResponseWithEmptyCarInstance()
        {
            string requestData = JsonConvert.SerializeObject(GetRandomCarFromDb());
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Post, "update",
                requestData);

            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }

        [Fact]
        public async Task UpdateResultHasCorrectErrorResponseWithWrongInstanceData()
        {
            string requestData = JsonConvert.SerializeObject(GetRandomCarFromDb());
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Post, "update",
                requestData);

            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }

        [Fact]
        public async Task UpdateResultCorrectlyUpdateNullValueDescription()
        {
            // Receiving all cars to check for update fields.
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Get, "getAll");

            Assert.True(responseMessage.IsSuccessStatusCode);

            // Reading response and getting first car from list.
            string response = await responseMessage.Content.ReadAsStringAsync();
            List<Car> carsList = JsonConvert.DeserializeObject<List<Car>>(response);
            
            Assert.True(carsList.Count > 0);
            
            Car car = carsList.First();

            // Saving old description to compare with update response.
            string oldDescription = car.Description;
            
            // Removing description and send update request.
            car.Description = null;

            // Removing null value from JSON ("Description = null" will have the same result.)
            string requestData = JsonConvert.SerializeObject(car, Formatting.None, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            
            responseMessage = await PerformRequestAsync(HttpMethod.Post, "update", requestData);
            
            Assert.True(responseMessage.IsSuccessStatusCode);

            // Getting updated car.
            responseMessage = await PerformRequestAsync(HttpMethod.Get, $"get/{car.Id}");
            response = await responseMessage.Content.ReadAsStringAsync();
            
            Car updatedCar = JsonConvert.DeserializeObject<Car>(response);
            
            Assert.True(updatedCar.Description == oldDescription);
        }

        [Fact]
        public async Task RemoveResultHasCorrectErrorResponse()
        {
            Car car = GetRandomCarFromDb();
            string requestData = JsonConvert.SerializeObject(car);
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Post, $"remove/{car.Id}",
                requestData);

            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }

        [Fact]
        public async Task RemoveResultHasCorrectErrorResponseWithWrongInstanceData()
        {
            Car car = GetRandomCarFromDb();
            
            string requestData = JsonConvert.SerializeObject(car);
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Post, $"remove/{car.Id}",
                requestData);
            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }

        [Fact]
        public async Task GetAllResultHasCorrectResponse()
        {
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Get, "getAll");
            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }
        
        [Fact]
        public async Task GetByIdResultHasCorrectErrorResponse()
        {            
            // Response should return error response, because API's database supports only string-based objectId.
            HttpResponseMessage responseMessage = await PerformRequestAsync(HttpMethod.Get, $"get/{GetRandomCarFromDb().Id}");
            Assert.True(await IsResponseCorrectAsync(responseMessage));
        }

        private async Task<bool> IsResponseCorrectAsync(HttpResponseMessage responseMessage)
        {
            bool isResponseCorrect = false;
            string response = await responseMessage.Content.ReadAsStringAsync();
            
            try
            {
                _testOutputHelper.WriteLine($"Response: {response} --- StatusCode: {responseMessage.StatusCode}");
                JsonConvert.DeserializeObject(response);
                isResponseCorrect = true;
            }
            catch (Exception ex)
            {
                _testOutputHelper.WriteLine($"Response: {response} --- Message: {ex.Message}");
                isResponseCorrect = false;
            }

            return isResponseCorrect;
        }

        private async Task<HttpResponseMessage> PerformRequestAsync(HttpMethod httpMethod, string endpoint, string postParams = null)
        {
            IWebHostBuilder webHostBuilder = new WebHostBuilder().UseStartup<Startup>();
                        
            TestServer server = new TestServer(webHostBuilder);
            HttpClient client = server.CreateClient();
            string requestUrl = $"{client.BaseAddress}api/cars/{endpoint}";
                        
            HttpRequestMessage requestMessage = new HttpRequestMessage()
            {
                RequestUri = new Uri(requestUrl)
            };
            
            if (httpMethod.Method == HttpMethod.Get.ToString())
            {
                requestMessage.Method = HttpMethod.Get;
            }
            else if (httpMethod.Method == HttpMethod.Post.ToString())
            {
                requestMessage.Method = HttpMethod.Post;

                if (postParams != null)
                {
                    requestMessage.Content = new StringContent(postParams, Encoding.UTF8, "application/json");
                }
            }

            return await client.SendAsync(requestMessage);
        }

        private void CreateTestDb()
        {
            CarsInDatabase = new List<Car>();
            
            for (int i = 0; i < 5; i++)
            {
                CarsInDatabase.Add(new Car()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                });
            }
        }

        private Car GetRandomCarFromDb()
        {
            int randomIndex = new Random().Next(0, CarsInDatabase.Count - 1);
            return CarsInDatabase[randomIndex];
        }
    }
}