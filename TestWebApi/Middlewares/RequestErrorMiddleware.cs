using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using TestWebApi.Database.Constants;
using TestWebApi.Database.Models;

namespace TestWebApi.Middlewares
{
    public class RequestErrorMiddleware
    {
        private readonly RequestDelegate _next;
        
        public RequestErrorMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        
        public async Task InvokeAsync(HttpContext context)
        {
            string requestMethod = context.Request.Method;
            string contentType = context.Request?.ContentType ?? "";
            bool isContentTypeJson = contentType.Contains("application/json");
                
            if (requestMethod == "POST" && !isContentTypeJson)
            {
                context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                    
                await context.Response.WriteAsync(JsonConvert.SerializeObject(new ResponseResult()
                {
                    Status = ResponseResultStatus.Error,
                    Message =
                        $"Unsupported Content-Type. Supported type is: application/json, passed type is: {contentType}"
                }));
            }
            else
            {
                try
                {
                    await _next(context);
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = (int) HttpStatusCode.BadRequest;

                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new ResponseResult()
                    {
                        Status = ResponseResultStatus.Error,
                        Message = ex.Message
                    }));
                }
            }
        }
    }
}