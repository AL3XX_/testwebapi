﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestWebApi.Database.Constants;
using TestWebApi.Database.Contexts.CommandContexts.Car;
using TestWebApi.Database.Contexts.QueryContexts.Car;
using TestWebApi.Database.Entities;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;
using TestWebApi.Database.RepositoryCommands;
using TestWebApi.Database.RepositoryCommands.Car;

namespace TestWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly ICommandBuilder _commandBuilder;

        private readonly IQueryBuilder _queryBuilder;

        public CarsController(ICommandBuilder commandBuilder, IQueryBuilder queryBuilder)
        {
            _commandBuilder = commandBuilder;
            _queryBuilder = queryBuilder;
        }

        [HttpGet]
        public string ShowApiGreetings()
        {
            return "Hello, this is the Test API. Reference you can see here -> https://bitbucket.org/AL3XX_/testwebapi/src/master/README.md";
        }
        
        // GET /api/cars/getAll
        [HttpGet("[action]")]
        public async Task<List<Car>> GetAll()
        {
            return await _queryBuilder.QueryAsync<GetCarsListQueryContext, List<Car>>(new GetCarsListQueryContext());;
        }

        // GET /api/cars/get/{id}
        [HttpGet("[action]/{id}")]
        public async Task<Car> Get(string id)
        {   
            return await _queryBuilder.QueryAsync<GetByIdQueryContext, Car>(new GetByIdQueryContext()
            {
                Id = id
            });
        }
        
        // POST /api/cars/create
        [HttpPost("[action]")]
        public async Task<CommandResult> Create([FromBody] Car car)
        {
            return await _commandBuilder.ExecuteAsync(new CreateCarCommandContext()
            {
                Car = car
            });
        }

        // POST /api/cars/update
        [HttpPost("[action]")]
        public async Task<CommandResult> Update([FromBody] Car car)
        {
            CommandResult result = new CommandResult();
            
            if (car.Id == null)
            {
                result = await _commandBuilder.ExecuteAsync(new CreateCarCommandContext()
                {
                    Car = car
                });
            }
            else
            {
                result = await _commandBuilder.ExecuteAsync(new UpdateCarCommandContext()
                {
                    Car = car
                });
            }

            return result;
        }

        // POST /api/cars/remove/{id}
        [HttpPost("[action]/{id}")]
        public async Task<CommandResult> Remove(string id)
        {
            return await _commandBuilder.ExecuteAsync(new RemoveCarCommandContext()
            {
                Id = id
            });
        }
    }
}