﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Newtonsoft.Json;
using TestWebApi.Database.Constants;
using TestWebApi.Database.Contexts.CommandContexts;
using TestWebApi.Database.Interfaces;
using TestWebApi.Database.Models;
using TestWebApi.Database.RepositoryCommands;
using TestWebApi.Database.RepositoryQueries;
using TestWebApi.Middlewares;

namespace TestWebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            services.Configure<ApiBehaviorOptions>(options =>
            {
                // Registering one common error response from API. 
                options.InvalidModelStateResponseFactory = context =>
                {
                    ValidationProblemDetails problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Instance = context.HttpContext.Request.Path
                    };

                    // Getting first (priority) error message and show it in response.
                    KeyValuePair<string, string[]> priorityError = problemDetails.Errors.FirstOrDefault();
                    string priorityErrorMessage = priorityError.Value.FirstOrDefault()?.ToString();

                    return new BadRequestObjectResult(new ResponseResult()
                    {
                        Status = ResponseResultStatus.Error,
                        Message = priorityErrorMessage
                    });
                };
            });
            
            ContainerBuilder builder = new ContainerBuilder();
            
            // Reference to Database project - IRepositoryCommand Assembly.
            Assembly assembly = typeof(IRepositoryCommand<>).Assembly;
            
            builder.RegisterAssemblyTypes(assembly).AsClosedTypesOf(typeof(IRepositoryCommand<>));
            builder.RegisterAssemblyTypes(assembly).AsClosedTypesOf(typeof(IRepositoryQuery<,>));

            builder.RegisterType<CommandBuilder>().As<ICommandBuilder>();
            builder.RegisterType<QueryBuilder>().As<IQueryBuilder>();
            
            builder.Populate(services);
            
            IContainer container = builder.Build();
            IServiceProvider serviceProvider = new AutofacServiceProvider(container);
            
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseStatusCodePages(async context =>
            {
                context.HttpContext.Response.ContentType = "application/json";
                ResponseResult responseResult = new ResponseResult()
                {
                    Status = ResponseResultStatus.Error,
                    Message = "Endpoint not found."
                };

                string result = JsonConvert.SerializeObject(responseResult);
                await context.HttpContext.Response.WriteAsync(result.ToString());
            });

            app.UseMiddleware<RequestErrorMiddleware>();
            app.UseMvc();
        }
    }
}