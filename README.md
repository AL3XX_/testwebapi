TestWebApi
API Reference
Car { "Id":"5c69b630860b278fa5e2a6cc", "Name":"", "Description":"" }
Id parameter in POST requests should be in string format as Mongo's objectId (5c69b630860b278fa5e2a6cc).

GET /api/cars/getAll - returns list of all created cars.
GET /api/cars/get/{id} - returns one car by mongo's objectId (example - 5c69b630860b278fa5e2a6cc).
POST /api/cars/create - takes car instance as JSON and create one car.
POST /api/cars/update - takes car instance as JSON and update one car.
POST /api/cars/remove - takes car Id as objectId and remove one car.